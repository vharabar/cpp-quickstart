CC		:= clang
C_FLAGS := -std=c++17 -Wall -Wextra  -fuse-ld=lld 
DEBUG_FLASG := -g  # flags for generating debug binary
LL := -cc1 -emit-llvm # flags for generating llvm IR

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:=

ifeq ($(OS),Windows_NT)
C_FLAGS := $(C_FLAGS) -fdelayed-template-parsing -fms-compatibility
DEBUG_FLASG := $(DEBUG_FLASG) -gcodeview
EXECUTABLE	:= main.exe
DEBUGGABLE := gmain.exe
else
EXECUTABLE	:= main
DEBUGGABLE := gmain
endif

all: $(BIN)/$(EXECUTABLE)

debug: $(BIN)/$(DEBUGGABLE)

clean:
	$(RM) $(BIN)/*

run: all
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*
	$(CC) $(C_FLAGS) -I$(INCLUDE) -L$(LIB) -O2 $^ -o $@ $(LIBRARIES)

$(BIN)/$(DEBUGGABLE): $(SRC)/*
	$(CC) $(C_FLAGS) $(DEBUG_FLASG) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)